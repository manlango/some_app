json.extract! quote, :id, :customer_id, :employee_id, :vehicle_id, :markup, :sales_tax, :total, :created_at, :updated_at
json.url quote_url(quote, format: :json)
